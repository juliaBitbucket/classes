<?php
require_once 'comments.php';

class News {
    private $fileName;
    private $title;
    private $text;
    private $date;
    private $author;

    public function __construct($fileName)
    {
        $this->fileName = $fileName;
        $this->getJson($fileName);
    }

    public function getTitle(){
        return $this->title;
    }

    public function getDate($format = 'd/m/Y'){
        return date($format, strtotime($this->date));
    }

    public function getAuthor(){
        return $this->author;
    }

    public function getText(){
        return $this->text;
    }

    public function setTitle($title){
        $this->title = $title;
    }

    public function setDate($date){
        $this->date = $date;
    }

    public function setAuthor($author){
        $this->author = $author;
    }

    public function setText($text){
        $this->text = $text;
    }

    public function getJson($fileName){
        $json = file_get_contents($fileName);
        $data = json_decode($json, true);
        $this->setTitle($data['title']);
        $this->setDate($data['date']);
        $this->setAuthor($data['author']);
        $this->setText($data['text']);
    }

    public function getComments(Comments $comments)
    {
        $comments->getJson($this->fileName);
        return $comments->getComments();
    }
}