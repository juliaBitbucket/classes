<?php
class Comments {
    private $comments;

    public function getComments(){
        return $this->comments;
    }

    public function setComments($comment){
        $this->comments = $comment;
    }

    public function getJson($fileName){
        $json = file_get_contents($fileName);
        $data = json_decode($json, true);
        if (!empty($data['comments'])) {
            $this->setComments($data['comments']);
        }
    }
}