<?php
/******************* Car *************************/
class Car {
    private $color = 'white';
    private $transmission = 'manual';
    private $fuel = 'petrol';
    private $year;


    public function __construct()
    {
        $this->year = date('Y-m-d');
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getTransmission()
    {
        return $this->transmission;
    }

    public function getFuel()
    {
        return $this->fuel;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setColor($value)
    {
        $this->color = $value;
    }

    public function setTransmission($value)
    {
        $this->transmission = $value;
    }

    public function setFuel($value)
    {
        $this->fuel = $value;
    }

    public function start() {
        echo 'Start a car.';
    }

    public function muffle() {
        echo 'Muffle car.';
    }
}

$carGreen = new Car();
$carGreen->setColor('green');

$carAuto = new Car();
$carAuto->setTransmission('auto');

/***************** TV ***************************/

class TV {
    private $size;
    private $category;
    private $year;


    public function __construct($size, $category)
    {
        $this->year = date('Y-m-d');
        $this->setSize($size);
        $this->setCategory($category);
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getYear()
    {
        return $this->year;
    }

    private function setSize($value)
    {
        $this->size = $value;
    }

    private function setCategory($value)
    {
        $this->category = $value;
    }

    public function turnOff() {
        echo 'TV turnOff.';
    }

    public function turnOn() {
        echo 'TV turnOff.';
    }
}

$tv = new TV(23, 'samsung');

$newTV = new TV(52, 'LG');

/***************** Ball pen ***************************/
class BallPen {
    private $color;
    private $made = 'plastic';
    private $size = 20;
    private $year;


    public function __construct($color)
    {
        $this->year = date('Y-m-d');
        $this->setColor($color);
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getMade()
    {
        return $this->made;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setColor($value)
    {
        $this->color = $value;
    }

    public function setMade($value)
    {
        $this->made = $value;
    }

    public function setSize($value)
    {
        $this->size = $value;
    }

    public function write() {
        echo 'Start write a ball pen.';
    }
}

$ballPen = new BallPen('red');

$newBallPen = new BallPen('black');
$newBallPen->setMade('wood');

/***************** Duck ***************************/
class Duck {
    private $made = 'wood';
    private $size = 'little';
    private $year;


    public function __construct()
    {
        $this->year = date('Y-m-d');
    }

    public function getMade()
    {
        return $this->made;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function setMade($value)
    {
        $this->made = $value;
    }

    public function setSize($value)
    {
        $this->size = $value;
    }

    public function swims() {
        echo 'Start swims a duck.';
    }
}

$duck = new Duck();

$newDuck = new Duck();
$newDuck->setMade('plastic');


/***************** Product ***************************/

class Product {
    private $brand = 'Hahn';
    private $type;
    private $category;
    private $color;
    private $year;


    public function __construct($type, $color, $category)
    {
        $this->year = date('Y-m-d');
        $this->setType($type);
        $this->setColor($color);
        $this->setCategory($category);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getYear()
    {
        return $this->year;
    }

    private function setType($value)
    {
        $this->type = $value;
    }

    private function setCategory($value)
    {
        $this->category = $value;
    }


    private function setColor($value)
    {
        $this->color = $value;
    }

    public function getPrice() {
        if ($this->category == 'quartz') {
            return 20;
        }
        return 30;
    }
}

$product = new Product('2cm', 'white', 'quartz');

$newProduct = new Product('3cm', 'crime', 'quartz');



/*print_r($carAuto);
print_r($carGreen);
print_r($newTV);
print_r($tv);
print_r($ballPen);
print_r($newBallPen);
print_r($duck);
print_r($newDuck);
print_r($product);
print_r($newProduct);*/