<?php
    require_once 'news.php';

    $folderNews = 'news';
    $catalog = array_diff(scandir(__DIR__ . "/$folderNews"), array('..', '.'));
    if (empty($catalog)) {
        echo 'Сегодня новости отсутствуют.';
        die;
    }

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Новости</title>
    <style  type="text/css">

    </style>
    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
    <body>
    <div class = "container">
        <div class = "row">
            <div class = "col-md-2"></div>
            <div class = "col-md-8">
                <h2>Новости</h2>
                <div class = "row">
                <?php foreach ($catalog as $file):
                        if (pathinfo($file, PATHINFO_EXTENSION) == 'json'):
                            $news = new News("$folderNews/" . $file); ?>
                    <h3><?php echo $news->getTitle(); ?></h3>
                    <p><?php echo $news->getText(); ?></p>
                    <p><strong>Разместил:</strong> <?php echo $news->getAuthor(); ?></p>
                    <p><strong>Дата:</strong> <?php echo $news->getDate(); ?></p>
                            <?php $comments = $news->getComments(new Comments());
                                if (!empty($comments)):
                                foreach($comments as $comment): ?>
                                    <h5>Коментарий: </h5>
                                    <p><?php echo $comment; ?></p>
                            <?php endforeach; endif; endif; endforeach; ?>
                </div>
            </div>
            <div class = "col-md-2"></div>
        </div>
    </div>
    </body>
</html>